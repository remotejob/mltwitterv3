package cleanstring

import (
	"regexp"
	"strings"
)

func Elab(text string) string {

	reg := regexp.MustCompile("[^\\p{L}\\d_?!.]+")

	preproceed := strings.Fields(text)

	var onlywords []string

	for _, prstr := range preproceed {

		if !strings.HasPrefix(prstr, "@") && !strings.HasPrefix(prstr, "https://") {

			prstr = strings.Replace(prstr, "\n", " ", -1)

			regprstr := reg.ReplaceAllString(prstr, " ")

			if len(regprstr) > 0 {

				onlywords = append(onlywords, regprstr)
			}

		}

	}

	clphrase := strings.Join(onlywords, " ")
	clphrase = strings.Replace(clphrase, "..", ".", -1)
	clphrase = strings.Replace(clphrase, "??", "?", -1)
	clphrase = strings.Replace(clphrase, "!!", "!", -1)
	clphrase = strings.Replace(clphrase, "  ", " ", -1)

	return strings.TrimSpace(clphrase)

}

func ElabOldBot(text string) string {

	// reg := regexp.MustCompile("[^\\p{L}\\d]+")
	reg := regexp.MustCompile("[\\p{L}]+")
	// reg := regexp.MustCompile("^[A-Za-z]+$")

	preproceed := strings.Fields(text)

	var onlywords []string

	for _, prstr := range preproceed {

		prstr = strings.Replace(prstr, "\n", " ", -1)

		regprstr := reg.ReplaceAllString(prstr, " ")

		if len(regprstr) > 0 {

			onlywords = append(onlywords, regprstr)
		}

	}

	clphrase := strings.Join(preproceed, " ")
	clphrase = strings.Replace(clphrase, ".", "", -1)
	clphrase = strings.Replace(clphrase, "?", "", -1)
	clphrase = strings.Replace(clphrase, "!", "", -1)
	clphrase = strings.Replace(clphrase, "  ", " ", -1)

	return strings.TrimSpace(clphrase)

}

func CleanForDic(reg *regexp.Regexp, word string) bool {

	if !reg.MatchString(word) && len(word) > 1 {

		return true

	}

	return false
}
