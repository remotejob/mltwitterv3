package dbhandler

import (
	"database/sql"
	"log"
	"strconv"

	"gitlab.com/remotejob/mltwitterv3/internal/domains"
)

func SelectOneKw(db *sql.DB, stmt string) (string, error) {

	var res string
	row := db.QueryRow(stmt)

	switch err := row.Scan(&res); err {
	case sql.ErrNoRows:
		return "", sql.ErrNoRows
	case nil:

		return res, nil
	default:

		return "", err
	}

}

func SelectOneInt(db *sql.DB, stmt string) (int64, error) {

	var res int64
	row := db.QueryRow(stmt)

	switch err := row.Scan(&res); err {
	case sql.ErrNoRows:
		return 0, sql.ErrNoRows
	case nil:

		return res, nil
	default:

		return 0, err
	}

}

func SelectKw(db *sql.DB, stmt string) (map[string]struct{}, error) {

	res := make(map[string]struct{}, 0)

	rows, err := db.Query(stmt)
	if err != nil {
		return nil, err

	}
	defer rows.Close()

	for rows.Next() {

		var kw string

		err = rows.Scan(&kw)
		if err != nil {
			// log.Panicln(err.Error()) // proper error handling instead of panic in your app
			return nil, err
		}

		res[kw] = struct{}{}

	}

	return res, nil

}

func CreateLabels(db *sql.DB, stmt string, entities map[int64]string) (map[int64]string, error) {

	res := make(map[int64]string)

	for k, _ := range entities {

		sqlstr := stmt + strconv.FormatInt(k, 10)

		rows, err := db.Query(sqlstr)
		if err != nil {
			return nil, err

		}
		var ids []int64
		for rows.Next() {

			var k int64

			err = rows.Scan(&k)
			if err != nil {
				return nil, err
			}
			ids = append(ids, k)

		}

		var labelstr string
		for _, lbid := range ids {

			labelstr = labelstr + "__label__" + strconv.FormatInt(lbid, 10) + " "
		}

		if labelstr != "" {

			res[k] = labelstr

		}

	}

	return res, nil
}

//http://hunterheidenreich.com/blog/google-universal-sentence-encoder-in-keras/
func CreateLabelsForSematicSimilarity(db *sql.DB, stmt string, entities map[int64]string) (map[int64]string, error) {

	res := make(map[int64]string)

	for k, _ := range entities {

		sqlstr := stmt + strconv.FormatInt(k, 10)

		rows, err := db.Query(sqlstr)
		if err != nil {
			return nil, err

		}
		var ids []int64
		for rows.Next() {

			var k int64

			err = rows.Scan(&k)
			if err != nil {
				return nil, err
			}
			ids = append(ids, k)

		}

		if len(ids) > 0 {

			label := strconv.FormatInt(ids[0], 10)
			res[k] = "L" + label + ":L" + label + " "
		}

	}

	return res, nil
}

func SelectRow(db *sql.DB, stmt string, id int) string {
	var res string

	err := db.QueryRow(stmt, id).Scan(&res)
	if err != nil {
		log.Fatal(err)
	}

	return res

}

func SelectRowHits(db *sql.DB, stmt string, rowid int64) (int64, error) {
	var res int64

	err := db.QueryRow(stmt, rowid).Scan(&res)
	if err != nil {
		log.Fatal(err)
		return res, err
	}

	return res, nil

}

func Select(db *sql.DB, stmt string, limit int) (map[int64]string, error) {

	res := make(map[int64]string, limit)

	sqlstr := stmt + strconv.Itoa(limit)
	rows, err := db.Query(sqlstr)
	if err != nil {
		return nil, err

	}
	defer rows.Close()
	for rows.Next() {

		var k int64
		var v string

		err = rows.Scan(&k, &v)
		if err != nil {
			// log.Panicln(err.Error()) // proper error handling instead of panic in your app
			return nil, err
		}

		res[k] = v

	}

	return res, nil
}

func SelectAllAsk(db *sql.DB, stmt string) (map[int64]string, error) {

	res := make(map[int64]string)

	rows, err := db.Query(stmt)
	if err != nil {
		return nil, err

	}
	defer rows.Close()
	for rows.Next() {

		var k int64
		var v string

		err = rows.Scan(&k, &v)
		if err != nil {
			// log.Panicln(err.Error()) // proper error handling instead of panic in your app
			return nil, err
		}

		res[k] = v

	}

	return res, nil
}

func UpdateKw(db *sql.DB, sqlstr string, entitie string) error {

	tx, err := db.Begin()
	if err != nil {
		return err

	}

	// for k, _ := range entities {

	stmt, err := db.Prepare(sqlstr)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(entitie)
	if err != nil {
		return err
	}

	// }
	err = tx.Commit()
	if err != nil {
		return err

	}

	return nil
}

func Insert(db *sql.DB, sqlstr string, entities []domains.Dialog) error {

	tx, err := db.Begin()
	if err != nil {
		return err

	}

	for _, v := range entities {

		stmt, err := db.Prepare(sqlstr)
		if err != nil {
			return err
		}
		defer stmt.Close()

		_, err = stmt.Exec(v.Ask, v.Answer, v.Guuid, v.Level)
		if err != nil {
			return err
		}

	}
	err = tx.Commit()
	if err != nil {
		return err

	}

	return nil

}

func SelectForCleanup(db *sql.DB, stmt string) (map[int64]domains.Dialog, error) {

	res := make(map[int64]domains.Dialog)

	rows, err := db.Query(stmt)
	if err != nil {

		return nil, err
	}

	defer rows.Close()
	for rows.Next() {

		var id int64
		var answer string
		var ask string
		var guuid string
		var level int
		err = rows.Scan(&id, &ask, &answer, &guuid, &level)
		if err != nil {

			return nil, err
		}

		res[id] = domains.Dialog{ask, answer, guuid, level}

	}

	return res, nil

}

func SelectForSeqSeq(db *sql.DB, stmt string) (map[int64]domains.Dialog, error) {

	res := make(map[int64]domains.Dialog)

	rows, err := db.Query(stmt)
	if err != nil {

		return nil, err
	}

	defer rows.Close()
	for rows.Next() {

		var id int64
		var answer string
		var ask string
		// var guuid string
		// var level int
		err = rows.Scan(&id, &ask, &answer)
		if err != nil {

			return nil, err
		}

		res[id] = domains.Dialog{
			Ask:    ask,
			Answer: answer,
		}

	}

	return res, nil

}

func InsertAllDialogs(db *sql.DB, stmt string, entities []domains.Dialog) error {

	tx, err := db.Begin()
	if err != nil {
		return err

	}

	for _, v := range entities {

		if v.Ask != "" && v.Answer != "" && v.Guuid != "" {

			stmtpr, err := db.Prepare(stmt)
			if err != nil {
				return err
			}
			defer stmtpr.Close()

			_, err = stmtpr.Exec(v.Ask, v.Answer, v.Guuid, v.Level)
			if err != nil {
				return err
			}
		}

	}

	err = tx.Commit()
	if err != nil {
		return err

	}

	return nil
}

func InsertIgnore(db *sql.DB, stmt0 string, stmt1 string, entities map[int64]domains.Dialog) error {

	for k, v := range entities {

		if len(v.Ask) > 5 && len(v.Answer) > 5 {

			stmt, err := db.Prepare(stmt0)
			if err != nil {
				return err
			}
			defer stmt.Close()

			rs, err := stmt.Exec(k, v.Ask, v.Guuid, v.Level)
			if err != nil {
				return err
			}

			askid, err := rs.LastInsertId()
			if err != nil {
				return err
			}

			if askid == 0 {

				askid = k

			}

			stmtAnswer, err := db.Prepare(stmt1)
			if err != nil {
				return err
			}
			defer stmtAnswer.Close()

			_, err = stmtAnswer.Exec(askid, v.Answer, v.Guuid, v.Level)
			if err != nil {
				return err
			}

		}

	}

	return nil
}

func UpdateOrgElab(db *sql.DB, sqlstr string, entities map[int64]domains.Dialog) error {

	tx, err := db.Begin()
	if err != nil {
		return err

	}

	for k, _ := range entities {
		stmt, err := db.Prepare(sqlstr)
		if err != nil {
			return err
		}
		defer stmt.Close()

		_, err = stmt.Exec(k)
		if err != nil {
			return err
		}

	}
	err = tx.Commit()
	if err != nil {
		return err

	}

	return nil

}

func UpdateOrgElabOtherLang(db *sql.DB, sqlstr string, entities map[int64]struct{}) error {

	tx, err := db.Begin()
	if err != nil {
		return err

	}

	for k, _ := range entities {
		stmt, err := db.Prepare(sqlstr)
		if err != nil {
			return err
		}
		defer stmt.Close()

		_, err = stmt.Exec(k)
		if err != nil {
			return err
		}

	}
	err = tx.Commit()
	if err != nil {
		return err

	}

	return nil

}

func UpdateAllKeywordElab(db *sql.DB, sqlstr string) error {

	tx, err := db.Begin()
	if err != nil {
		return err

	}

	stmt, err := db.Prepare(sqlstr)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec()
	if err != nil {
		return err
	}

	err = tx.Commit()
	if err != nil {
		return err

	}

	return nil

}
