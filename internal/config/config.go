package config

import (
	"database/sql"
	"fmt"
	"log"

	"github.com/fsnotify/fsnotify"
	_ "github.com/mattn/go-sqlite3"
	"github.com/spf13/viper"
)

type Constants struct {
	Twiiter struct {
		ConsumerKey    string
		ConsumerSecret string
		AccessToken    string
		AccessSecret   string
		// DBName string

	}

	FinBotDb struct {
		Url   string
		Limit int

	}


}
type Config struct {
	Constants
	// KwDb *sql.DB
	FbDb *sql.DB

}

func New() (*Config, error) {
	config := Config{}
	constants, err := initViper()
	config.Constants = constants
	if err != nil {
		return &config, err
	}


	finBotDb, err := sql.Open("sqlite3", config.Constants.FinBotDb.Url)
	if err != nil {
		return &config, err
	}

	config.FbDb = finBotDb

	return &config, err
}
func initViper() (Constants, error) {
	viper.SetConfigName("mltwitterv3.config") // Configuration fileName without the .TOML or .YAML extension
	viper.AddConfigPath(".")                    // Search the root directory for the configuration file
	err := viper.ReadInConfig()                 // Find and read the config file
	if err != nil {                             // Handle errors reading the config file
		return Constants{}, err
	}
	viper.WatchConfig() // Watch for changes to the configuration file and recompile
	viper.OnConfigChange(func(e fsnotify.Event) {
		fmt.Println("Config file changed:", e.Name)
	})
	if err = viper.ReadInConfig(); err != nil {
		log.Panicf("Error reading config file, %s", err)
	}
	var constants Constants

	err = viper.Unmarshal(&constants)
	return constants, err
}
