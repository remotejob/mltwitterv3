package main

import (
	"log"
	"os"
	"regexp"
	"strings"

	"github.com/abadojack/whatlanggo"
	"gitlab.com/remotejob/mltwitterv3/internal/config"
	"gitlab.com/remotejob/mltwitterv3/pkg/cleanstring"
	"gitlab.com/remotejob/mltwitterv3/pkg/dbhandler"
)

var (
	configuration *config.Config
	err           error
	tofile        []string
)

func init() {

	configuration, err = config.New()
	if err != nil {
		log.Panicln("Configuration error", err)
	}

}
func main() {

	askfb, err := os.OpenFile("data/ask.txt", os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		log.Fatalf("failed opening file: %s", err)
	}
	defer askfb.Close()

	answerfb, err := os.OpenFile("data/answer.txt", os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		log.Fatalf("failed opening file: %s", err)
	}
	defer answerfb.Close()

	tofile, err := dbhandler.SelectForSeqSeq(configuration.FbDb, "select rowid,ask,answer from dialogsorg")
	if err != nil {
		log.Fatalln(err)
		return
	}

	reask := regexp.MustCompile("[^a-zA-ZäöÄÖ]+")
	reanswer := regexp.MustCompile("[^a-zA-ZäöÄÖ?!.]+")

	for _, dialog := range tofile {

		ask := cleanstring.Elab(dialog.Ask)
		answer := cleanstring.Elab(dialog.Answer)

		ask = reask.ReplaceAllString(ask, " ")
		answer = reanswer.ReplaceAllString(answer, " ")


		ask = strings.TrimSpace(ask)
		ask = strings.ToLower(ask)
		answer = strings.TrimSpace(answer)

		info := whatlanggo.Detect(ask + " " + answer)

		chlang := whatlanggo.LangToString(info.Lang)

		if chlang == "fin" {

			asknwords := len(strings.Fields(ask))
			answords := len(strings.Fields(answer))

			// if (asknwords > 2 && asknwords < 31) && (answords > 2 && answords < 31) {
			// if (asknwords > 1 && asknwords < 35) && (answords > 1 && answords < 35) {
			if (asknwords > 1 && asknwords < 16) && (answords > 1 && answords < 16) {	

				_, err := askfb.WriteString(ask + "\n")
				if err != nil {
					log.Fatalf("failed writing to file: %s", err)
				}

				_, err = answerfb.WriteString(answer + "\n")
				if err != nil {
					log.Fatalf("failed writing to file: %s", err)
				}

			}

		}
	}

}
