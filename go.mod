module gitlab.com/remotejob/mltwitterv3

go 1.14

require (
	github.com/abadojack/whatlanggo v1.0.1
	github.com/fsnotify/fsnotify v1.4.9
	github.com/mattn/go-sqlite3 v1.14.4
	github.com/spf13/viper v1.7.1
)
